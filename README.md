******* Design Management Chart from single table *****

***** Organization Chart  from single table *****


Language: PHP 
Database: MYSQL**


Design Example : https://gitlab.com/uploads/-/system/project/avatar/31611425/Screen_Shot_2021-11-25_at_8.52.49_PM.png?width=400

**Features**:
1. Database Abstract class
2. Management/Position Class extends Database
3. Graph/Chart integrated
4. Simple code in PHP
5. Using Reference Assignment to a variable "&"  for eg: https://www.php.net/manual/en/language.oop5.references.php



**Instructions**:
1. Download and extract in your apache folder
2. Run SQL file
3. Set DB configuration details for your database in database.php file
4. Run with index.php file


**Usage**:
1. Use in a multilevel company
2. School PHP project
3. Computer Science college project

