CREATE TABLE `setup_positions` (
  `position_id` int UNSIGNED NOT NULL,
  `name` varchar(100) NOT NULL,
  `title` varchar(500) NOT NULL,
  `reporting_position_id` int DEFAULT NULL
);

INSERT INTO `setup_positions` (`position_id`, `name`, `title`, `reporting_position_id`) VALUES
(1, 'Akshay Rana', 'Managing Director', 0),
(2, 'Anthony Mackie', 'Group Operations Director', 1),
(3, 'John Wick', 'Group Commercial Director', 2),
(4, 'Ravi Kumar', 'Business Strategy Director', 1),
(5, 'Thor', 'Director of Swimming', 2),
(6, 'Hassan Ali', 'Executive Assistant to MD', 5);

ALTER TABLE `setup_positions`
  ADD PRIMARY KEY (`position_id`),
  ADD UNIQUE KEY `setup_positions_position_id_unique` (`position_id`);

  ALTER TABLE `setup_positions`
  MODIFY `position_id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;