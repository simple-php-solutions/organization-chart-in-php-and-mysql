<?php 

include('database.php');
include('position.php');

$poistion = new Position();

$levels = $poistion->getLevels();

?>



<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Organization Chart Plugin</title>
  <link rel="icon" href="https://dabeng.github.io/OrgChart/img/logo.png">
  <link rel="stylesheet" href="https://dabeng.github.io/OrgChart/css/jquery.orgchart.css">
  <link rel="stylesheet" href="https://dabeng.github.io/OrgChart/css/style.css">
</head>
<body>
  <div id="chart-container"></div>

  <script type="text/javascript" src="https://dabeng.github.io/OrgChart/js/jquery.min.js"></script>
  <script type="text/javascript" src="https://dabeng.github.io/OrgChart/js/jquery.orgchart.js"></script>
  <script type="text/javascript">
    $(function() {

    /*
    SAMPLE DATA
    var datasource = {
      'name': 'Lao Lao',
      'title': 'general manager',
      'children': [
        { 'name': 'Bo Miao', 'title': 'department manager' },
        { 'name': 'Su Miao', 'title': 'department manager',
          'children': [
            { 'name': 'Tie Hua', 'title': 'senior engineer' },
            { 'name': 'Hei Hei', 'title': 'senior engineer',
              'children': [
                { 'name': 'Dan Dan', 'title': 'engineer' }
              ]
            },
            { 'name': 'Pang Pang', 'title': 'senior engineer' }
          ]
        },
        { 'name': 'Hong Miao', 'title': 'department manager' }
      ]
    }; */

   var datasource = <?php echo $levels; ?>;
 
    $('#chart-container').orgchart({
      'data' : datasource,
      'nodeContent': 'title'
    });

  });
  </script>
  </body>
</html>