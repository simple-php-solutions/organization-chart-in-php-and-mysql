<?php 
Abstract class Database
{
    private $dbHost;
    private $dbUser;
    private $dbPass;
    private $dbName;
    private $connection;

    function __construct() {
        $this->dbHost = 'localhost';
        $this->dbName = 'test';
        $this->dbUser = 'root';
        $this->dbPass = 'password';

        $this->connection = new mysqli($this->dbHost, $this->dbUser, $this->dbPass,$this->dbName);
    }

    function execute($query){

        return $this->connection->query($query)->fetch_all(MYSQLI_ASSOC);

    }

}
