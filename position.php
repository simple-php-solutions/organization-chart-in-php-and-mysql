<?php 

Class Position extends Database {


    function getLevels(){

        $positions = array();

        $sql = "SELECT * FROM setup_positions order by reporting_position_id";

        $rows = $this->execute($sql);

        $levels = array();
        $users = array();
        foreach( $rows as $row ) { 
 
            if($row["reporting_position_id"] == 0){
                $levels = $row;
                $users[ $row["position_id"] ] = &$levels['children'];
            }else{
                $pointer = &$users[ $row["reporting_position_id"] ];    
                $pointer[ $row["position_id"] ] = $row;
                $users[$row["position_id"]] = &$pointer[ $row["position_id"] ]['children'];  
            }
        }

        $levels = $this->designArrayForGraph($levels);
     
        return json_encode($levels,true) ;
    }


    function designArrayForGraph($levels){
        $return['name'] = $levels['name'] ;
        $return['title'] = $levels['title'];
        $return['reporting_position_id'] = $levels['reporting_position_id'];
        $return['children']  = $this->getChildren($levels['children']);
        return $return;
    }
    

    function getChildren($childrens){
        
        foreach($childrens as $children){
           
            if(!empty($children['children'])){
                $pointer = &$children;       
                $pointer['children'] = $this->getChildren($children['children']);
                $return[] = $pointer;
            }else{
                $return[] = $children;
            }
        }

        return $return;
    }

}